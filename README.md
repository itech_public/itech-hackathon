
# git - the simple guide
# just a simple guide for getting started with git. no deep shit ;)
http://rogerdudler.github.io/git-guide/

# semantic commit messages
https://seesparkbox.com/foundry/semantic_commit_messages

chore: add Oyster build script
docs: explain hat wobble
feat: add beta sequence
fix: remove broken confirmation message
refactor: share logic between 4d3d3d3 and flarhgunnstow
style: convert tabs to spaces
test: ensure Tayne retains clothing

# Create a project in Gitlab
https://docs.gitlab.com/ee/gitlab-basics/create-project.html

# Install Gitlab runner
https://docs.gitlab.com/runner/install/

# Register Gitlab runner
https://docs.gitlab.com/runner/register/index.html

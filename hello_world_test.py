__author__ = 'heiko'


import unittest
from hello_world import Greeter

class MyTestCase(unittest.TestCase):
    def test_greeting(self):
        greeter = Greeter()
        self.assertEqual(greeter.message, 'Hello world!')

    def test_greeting_fails(self):
        greeter = Greeter()
        self.assertEqual(greeter.message, 'Hallo Welt!')

if __name__ == '__main__':
    unittest.main()
